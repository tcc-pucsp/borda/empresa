package projeto.pucsp.tcc.borda.empresa.entidade;


import lombok.Data;
import lombok.Getter;
import lombok.Value;
import net.bytebuddy.utility.RandomString;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Data
@Value
@Getter
public class Token {

	private  String token;


	public  Token() {
		token = RandomString.make(100);
		new BCryptPasswordEncoder().encode(token);
	}
}
