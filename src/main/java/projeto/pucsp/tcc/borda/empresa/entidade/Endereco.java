package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.LastModifiedDate;
import projeto.pucsp.tcc.borda.empresa.dto.EnderecoDto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table( name = "endereco" )
@Data
@NoArgsConstructor
public class Endereco {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotBlank
	private String cep;

	@NotBlank
	private String logradouro;

	@NotBlank
	private String complemento;

	@NotNull
	private Integer numero;

	private Double latitude;

	private Double longitude;

	@LastModifiedDate
	@Column(name = "data_cadastro")
	private LocalDateTime dataCadastro;

	@LastModifiedDate
	@Column(name = "data_atualizacao")
	private Timestamp dataAtualizacao;


	@NotBlank
	private String cidade;

	@NotBlank
	private String estado;


	Endereco(EnderecoDto enderecoDto){

		this.cep = formatarCep(enderecoDto.getCep());

		this.logradouro = enderecoDto.getLogradouro();

		this.complemento = enderecoDto.getComplemento();

		this.numero = enderecoDto.getNumero();

		this.dataCadastro = LocalDateTime.now();

		this.dataAtualizacao = Timestamp.valueOf(LocalDateTime.now());

		this.cidade = enderecoDto.getCidade();

		this.estado = enderecoDto.getEstado();
	}

	private String formatarCep( String cep ) {

		return cep.replace("-", "")
				.replace(".", "")
				.substring( 0, 5 )
				.concat( "-" )
				.concat( cep.substring( 5, cep.length() ) );



	}

	public void aualizarGeolocalizacoa(Geolocalizacao geolocalizacao){

		latitude = geolocalizacao.getLatitude();
		longitude = geolocalizacao.getLongitude();

	}

}
