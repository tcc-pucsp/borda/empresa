package projeto.pucsp.tcc.borda.empresa.servico;

import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.borda.empresa.dto.EmpresaDto;
import projeto.pucsp.tcc.borda.empresa.entidade.Empresa;
import projeto.pucsp.tcc.borda.empresa.excecao.EmpresaJaCadastradaException;
import projeto.pucsp.tcc.borda.empresa.repositorio.Repositorio;

import java.util.Optional;

@Service
public class ServicoCadastra {

	private final Repositorio repositorio;




	public ServicoCadastra(Repositorio repositorio) {
		this.repositorio = repositorio;

	}


	public Empresa cadastar(EmpresaDto empresaDto) {

		Empresa empresa = new Empresa(empresaDto);

		Optional<Empresa> empresaCadastrado = repositorio.findEmpresaByCnpjRegistro(empresaDto.getCnpj().getRegistro());

		if (empresaCadastrado.isEmpty()) {
			return cadastarEmpresa(empresa);
		}

		return reativar(empresaCadastrado.get(), empresaDto);

	}

	private Empresa cadastarEmpresa(Empresa empresa) {

		return repositorio.save(empresa) ;
	}


	private Empresa reativar(Empresa empresa, EmpresaDto empresaDto){

		if (!empresa.encerrado()){

			throw new EmpresaJaCadastradaException
					("Empresa {} Ja Cadastrado".replace("{}", empresaDto.getNomeFantasia()));

		}
		empresa.reativar(empresaDto);

		return repositorio.save(empresa);

	}

}
