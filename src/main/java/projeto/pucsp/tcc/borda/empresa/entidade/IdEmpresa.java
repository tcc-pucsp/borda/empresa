package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.Value;

@Value
public class IdEmpresa {

	Integer id;

	public IdEmpresa(Empresa empresa){

		id = empresa.getId();

	}

}
