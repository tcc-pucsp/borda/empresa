package projeto.pucsp.tcc.borda.empresa.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rabbit.empresarial.geolocalizacao")
public class ProprieadadesMsgGeolocalizacao {

	private String topicExchange;

}
