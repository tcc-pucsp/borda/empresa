package projeto.pucsp.tcc.borda.empresa.servico;

import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.borda.empresa.entidade.Endereco;
import projeto.pucsp.tcc.borda.empresa.entidade.Geolocalizacao;
import projeto.pucsp.tcc.borda.empresa.excecao.EndercoNaoEncontrado;
import projeto.pucsp.tcc.borda.empresa.recurso.RecursoGeolocalizacao;
import projeto.pucsp.tcc.borda.empresa.repositorio.RepositorioEndereco;

@Service
public class ServicoGeolocalizacao implements RecursoGeolocalizacao {


	private  final RepositorioEndereco repositorioEndereco;


	public ServicoGeolocalizacao(RepositorioEndereco repositorio) {
		this.repositorioEndereco = repositorio;
	}


	@Override
	public void salvarEndereco(Geolocalizacao geolocalizacao) {


		final Endereco endereco = repositorioEndereco.findById(geolocalizacao.getId()).orElseThrow(() ->

				new EndercoNaoEncontrado(geolocalizacao.getId().toString())

		);
		endereco.aualizarGeolocalizacoa(geolocalizacao);

		repositorioEndereco.save(endereco);

	}
}
