package projeto.pucsp.tcc.borda.empresa.notificacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import projeto.pucsp.tcc.borda.empresa.entidade.Resposta;
import projeto.pucsp.tcc.borda.empresa.enumeracao.Mensagens;
import projeto.pucsp.tcc.borda.empresa.excecao.EmpresaJaCadastradaException;
import projeto.pucsp.tcc.borda.empresa.excecao.EmpresaNaoEncontrada;
import projeto.pucsp.tcc.borda.empresa.excecao.EndercoNaoEncontrado;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
@Slf4j
public class Notificacao {


	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler({EmpresaJaCadastradaException.class})
	public Resposta empresaJaCadastrada(EmpresaJaCadastradaException e) {

		log.info("Empresa ja Cadastrada");

		return new Resposta(Mensagens.EMPRESA_JA_CADASTRADA, e.getMessage());

	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({EmpresaNaoEncontrada.class})
	public Resposta empresaNaoEncontrada(EmpresaNaoEncontrada e) {

		log.info("Empresa Não Encontrada");

		return new Resposta(Mensagens.EMPRESA_NAO_ENCONTRADA, e.getMessage());

	}

	@ResponseStatus( HttpStatus.BAD_REQUEST )
	@ExceptionHandler( {ConstraintViolationException.class} )
	public Resposta dadosInvalidos( ConstraintViolationException e ){

		log.info("Dados Invalidos");

		return new Resposta( Mensagens.DADOS_INVALIDOS, e.getMessage());

	}

	@ResponseStatus( HttpStatus.NOT_FOUND )
	@ExceptionHandler( {EndercoNaoEncontrado.class} )
	public Resposta enderecoNaoEncontrado(EndercoNaoEncontrado e ){

		log.info("Endereco Não Encontrada");

		return new Resposta( Mensagens.DADOS_INVALIDOS, e.getMessage());

	}



}
