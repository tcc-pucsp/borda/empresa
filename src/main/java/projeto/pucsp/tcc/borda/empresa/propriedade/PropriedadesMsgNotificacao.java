package projeto.pucsp.tcc.borda.empresa.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rabbit.notificacao")
public class PropriedadesMsgNotificacao {

	private String topicExchange;


}
