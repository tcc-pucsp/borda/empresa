package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class EnderecoPublicacao  {


	private Integer id;

	private String cep;

	private String logradouro;

	private Integer numero;

	private String cidade;

	private String estado;


	public EnderecoPublicacao(Endereco endereco) {

		this.cep = endereco.getCep();
		this.logradouro = endereco.getLogradouro();
		this.numero = endereco.getNumero();
		this.id = endereco.getId();
		this.cidade = endereco.getCidade();
		this.estado = endereco.getEstado();


	}
}
