package projeto.pucsp.tcc.borda.empresa.recurso;

import projeto.pucsp.tcc.borda.empresa.entidade.Empresa;
import projeto.pucsp.tcc.borda.empresa.entidade.Geolocalizacao;

public interface RecursoToken {

	void salvarToken(Geolocalizacao geolocalizacao);

	void enviarNotificacaoToken(Empresa empresa);



}
