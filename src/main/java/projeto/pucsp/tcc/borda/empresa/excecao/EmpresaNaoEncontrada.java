package projeto.pucsp.tcc.borda.empresa.excecao;


import lombok.Getter;
import projeto.pucsp.tcc.borda.empresa.enumeracao.Mensagens;


@Getter
public class EmpresaNaoEncontrada extends RuntimeException {

    private static final long serialVersionUID = 6550477725682926573L;

    private final Mensagens mensagens;


    public EmpresaNaoEncontrada(String mensg) {

        super(mensg);

        this.mensagens = Mensagens.EMPRESA_NAO_ENCONTRADA;

    }
}
