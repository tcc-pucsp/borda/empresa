package projeto.pucsp.tcc.borda.empresa.enumeracao;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor( access = AccessLevel.PRIVATE )
@Getter
@JsonFormat( shape = JsonFormat.Shape.OBJECT )
public enum Mensagens {

	EMPRESA_JA_CADASTRADA( 409, "EMPRESA JA CADASTRADA" ),

	EMPRESA_NAO_ENCONTRADA(404, "EMPRESA NAO ENCONTRADA"),

	ENDERECO_NAO_ENCONTRADO(404, "EMPRESA NAO ENCONTRADA"),

	DADOS_INVALIDOS( 400, "DADOS INVALIDOS");

	private final Integer codigo;

	private final String contexto;


}


