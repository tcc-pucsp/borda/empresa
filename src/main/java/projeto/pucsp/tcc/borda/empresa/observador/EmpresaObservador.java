package projeto.pucsp.tcc.borda.empresa.observador;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.borda.empresa.entidade.Geolocalizacao;
import projeto.pucsp.tcc.borda.empresa.recurso.RecursoGeolocalizacao;
import projeto.pucsp.tcc.borda.empresa.servico.ServicoGeolocalizacao;
import projeto.pucsp.tcc.borda.empresa.servico.ServicoTocken;

@Component
public class EmpresaObservador implements RecursoGeolocalizacao  {

	private  final ServicoGeolocalizacao servicoGeolocalizacao;
	private final ServicoTocken servicoTocken;

	public EmpresaObservador(ServicoGeolocalizacao servicoGeolocalizacao, ServicoTocken servicoTocken) {
		this.servicoGeolocalizacao = servicoGeolocalizacao;
		this.servicoTocken = servicoTocken;
	}

	@RabbitListener(queues = "empresarial.endereco")
	@Override
	public void salvarEndereco(Geolocalizacao geolocalizacao) {

		servicoGeolocalizacao.salvarEndereco(geolocalizacao);
		servicoTocken.salvarToken(geolocalizacao);

	}

}
