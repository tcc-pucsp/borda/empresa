package projeto.pucsp.tcc.borda.empresa.dto;

import lombok.Builder;
import lombok.Value;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
@Value
public class EmpresaDto {

	@NotBlank
	private String nomeEmpresa;

	@NotBlank
	private String razaoSocial;

	@NotBlank
	@Size(max = 15)
	private String telefone;

	@NotBlank
	private String nomeFantasia;

	@NotBlank
	@Email
	private String email;

	@NotBlank
	private CnpjDto cnpj;

	@NotBlank
	private EnderecoDto endereco;


}
