package projeto.pucsp.tcc.borda.empresa.recurso;

import projeto.pucsp.tcc.borda.empresa.dto.EmpresaDto;
import projeto.pucsp.tcc.borda.empresa.entidade.IdEmpresa;
import projeto.pucsp.tcc.borda.empresa.proxy.EmpresaProxy;

public interface RecursoEmpresa {

	IdEmpresa cadastrarEmpresa(EmpresaDto empresaDTO);

	EmpresaProxy obterEmpresa(String cnpj);

	IdEmpresa encerrarEmpresa(String cnpj);

	IdEmpresa atualizarEmpresa(String cnpj, EmpresaDto empresaDto);



}
