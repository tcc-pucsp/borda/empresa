package projeto.pucsp.tcc.borda.empresa.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.borda.empresa.cliente.ClienteGeolocalizcao;
import projeto.pucsp.tcc.borda.empresa.dto.EmpresaDto;
import projeto.pucsp.tcc.borda.empresa.entidade.Empresa;
import projeto.pucsp.tcc.borda.empresa.entidade.EnderecoPublicacao;
import projeto.pucsp.tcc.borda.empresa.entidade.IdEmpresa;
import projeto.pucsp.tcc.borda.empresa.excecao.EmpresaNaoEncontrada;
import projeto.pucsp.tcc.borda.empresa.proxy.EmpresaProxy;
import projeto.pucsp.tcc.borda.empresa.recurso.RecursoEmpresa;
import projeto.pucsp.tcc.borda.empresa.repositorio.Repositorio;

@Slf4j
@Service
public class Servico implements RecursoEmpresa {

	private final Repositorio repositorio;
	private final ServicoCadastra servicoCadastra;
	private final ClienteGeolocalizcao clienteGeolocalizcao;

	Servico(Repositorio repositorio, ServicoCadastra servicoCadastra, ClienteGeolocalizcao clienteGeolocalizcao) {

		this.repositorio = repositorio;
		this.servicoCadastra = servicoCadastra;
		this.clienteGeolocalizcao = clienteGeolocalizcao;
	}

	@Override
	public IdEmpresa cadastrarEmpresa(EmpresaDto empresaDTO) {

		Empresa empresaCadastrada = servicoCadastra.cadastar(empresaDTO);

		clienteGeolocalizcao.publicar(new EnderecoPublicacao(empresaCadastrada.getEndereco()));


		return new IdEmpresa(empresaCadastrada);

	}

	@Override
	public EmpresaProxy obterEmpresa(String cnpj) {

		return repositorio.findEmpresaByCnpjRegistroAndDataEncerramentoIsNull(cnpj).orElseThrow(
				() -> new EmpresaNaoEncontrada("Empresa nao encontrada"));
	}

	@Override
	public IdEmpresa encerrarEmpresa(String cnpj) {

		Empresa empresa = buscarEmpresa(cnpj);
		if (!empresa.encerrado()) {

			empresa.encerrar();
			return new IdEmpresa (repositorio.save(empresa));

		}

		throw new EmpresaNaoEncontrada(empresa.getId().toString());
	}

	@Override
	public IdEmpresa atualizarEmpresa(String cnpj, EmpresaDto empresaDto) {

		Empresa empresa = repositorio.findEmpresaByCnpjRegistro(cnpj).orElseThrow(
				()->new EmpresaNaoEncontrada(cnpj));

		if (empresa.encerrado()){

			throw new EmpresaNaoEncontrada(cnpj);

		}

		empresa.atualizarEmpresa(empresaDto);

		Empresa empresaSalva = repositorio.save(empresa);

		clienteGeolocalizcao.publicar(new EnderecoPublicacao(empresa.getEndereco()));

		return  new IdEmpresa(empresaSalva);
	}


	private Empresa buscarEmpresa(String cnpj) {

		return repositorio.findEmpresaByCnpjRegistro(cnpj).orElseThrow(
				() -> new EmpresaNaoEncontrada("Empresa nao encontrada"));

	}

}
