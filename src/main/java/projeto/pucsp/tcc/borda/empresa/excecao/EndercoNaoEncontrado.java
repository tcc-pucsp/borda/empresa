package projeto.pucsp.tcc.borda.empresa.excecao;

import lombok.Getter;
import projeto.pucsp.tcc.borda.empresa.enumeracao.Mensagens;

@Getter
public class EndercoNaoEncontrado extends RuntimeException {

	private static final long serialVersionUID = 6550477725682926573L;


	private final Mensagens mensagens;

	public EndercoNaoEncontrado(String msg) {
		super(msg);

		this.mensagens = Mensagens.ENDERECO_NAO_ENCONTRADO;

	}



}
