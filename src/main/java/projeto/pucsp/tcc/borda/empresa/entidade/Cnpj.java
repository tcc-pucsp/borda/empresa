package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CNPJ;
import projeto.pucsp.tcc.borda.empresa.dto.CnpjDto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table( name = "cnpj" )
@Data
@NoArgsConstructor
public class Cnpj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotBlank
	@CNPJ
	private String registro;


	public Cnpj(CnpjDto cnpjDto){

		this.registro = cnpjDto.getRegistro();
	}

}
