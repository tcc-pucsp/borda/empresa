package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.Data;

@Data
public class Geolocalizacao {

	private Integer id ;

	private Double latitude;

	private Double longitude;

	private String endereco;
}
