package projeto.pucsp.tcc.borda.empresa.proxy;

import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;

public interface EmpresExcluirProxy{

	String getEmail();

	@Value("#{target.nomeFantasia}")
	String getNomeFantasia();

	@Value("#{target.dataEncerramento}")
	LocalDateTime getDataEncerramento();


}
