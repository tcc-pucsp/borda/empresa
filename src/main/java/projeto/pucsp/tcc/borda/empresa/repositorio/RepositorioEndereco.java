package projeto.pucsp.tcc.borda.empresa.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import projeto.pucsp.tcc.borda.empresa.entidade.Endereco;

public interface RepositorioEndereco extends JpaRepository<Endereco, Integer> {


}
