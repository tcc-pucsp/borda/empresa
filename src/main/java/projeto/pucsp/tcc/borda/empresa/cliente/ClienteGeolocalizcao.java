package projeto.pucsp.tcc.borda.empresa.cliente;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.borda.empresa.entidade.EnderecoPublicacao;
import projeto.pucsp.tcc.borda.empresa.propriedade.ProprieadadesMsgGeolocalizacao;

@Component
@Slf4j
public class ClienteGeolocalizcao {

	private final RabbitTemplate rabbitTemplate;

	private final ProprieadadesMsgGeolocalizacao proprieadadesMsgGeolocalizacao;


	public ClienteGeolocalizcao(RabbitTemplate rabbitTemplate, ProprieadadesMsgGeolocalizacao proprieadadesMsgGeolocalizacao) {

		this.rabbitTemplate = rabbitTemplate;
		this.proprieadadesMsgGeolocalizacao = proprieadadesMsgGeolocalizacao;

	}

	public void publicar(EnderecoPublicacao enderecoPublicacao){

		rabbitTemplate.convertAndSend(proprieadadesMsgGeolocalizacao.getTopicExchange(),enderecoPublicacao);

		log.info("Consegui publicar essa porra");
	}
}
