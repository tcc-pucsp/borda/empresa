package projeto.pucsp.tcc.borda.empresa.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.borda.empresa.cliente.ClienteNotificacao;
import projeto.pucsp.tcc.borda.empresa.entidade.Email;
import projeto.pucsp.tcc.borda.empresa.entidade.Empresa;
import projeto.pucsp.tcc.borda.empresa.entidade.Geolocalizacao;
import projeto.pucsp.tcc.borda.empresa.entidade.Token;
import projeto.pucsp.tcc.borda.empresa.excecao.EmpresaNaoEncontrada;
import projeto.pucsp.tcc.borda.empresa.recurso.RecursoToken;
import projeto.pucsp.tcc.borda.empresa.repositorio.Repositorio;

@Service
@Slf4j
public class ServicoTocken implements RecursoToken {

	private final Repositorio repositorio;
	private static final String ASSUNTO = "Token de Acesso";
	private final ClienteNotificacao clienteNotificacao;


	public ServicoTocken(Repositorio repositorio, ClienteNotificacao clienteNotificacao) {
		this.repositorio = repositorio;
		this.clienteNotificacao = clienteNotificacao;
	}

	@Override
	public void salvarToken(Geolocalizacao geolocalizacao) {

		final Token token = new Token();


		final Empresa empresa = repositorio.findEmpresaByEnderecoId(geolocalizacao.getId()).orElseThrow(() ->

				new EmpresaNaoEncontrada(geolocalizacao.getId().toString())

		);

		empresa.registarToken(token);

		empresa.ativarRegistro();

		repositorio.save(empresa);

		enviarNotificacaoToken(empresa);
		log.info("tentado publicar uma notifcacao");
	}

	@Override
	public void enviarNotificacaoToken(Empresa empresa) {

		clienteNotificacao.publicar(construirNotificacao(empresa));

	}


	private Email construirNotificacao(Empresa empresa){

		return   Email
				.builder()
				.assunto(ASSUNTO)
				.endereco(empresa.getEmail())
				.mensagem(empresa.getToken())
				.build();


	}
}
