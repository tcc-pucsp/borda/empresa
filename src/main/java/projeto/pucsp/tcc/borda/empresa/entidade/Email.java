package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Email {

	String endereco;

	String mensagem;

	String assunto;

}
