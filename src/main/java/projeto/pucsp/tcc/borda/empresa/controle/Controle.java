package projeto.pucsp.tcc.borda.empresa.controle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import projeto.pucsp.tcc.borda.empresa.dto.EmpresaDto;
import projeto.pucsp.tcc.borda.empresa.entidade.IdEmpresa;
import projeto.pucsp.tcc.borda.empresa.proxy.EmpresaProxy;
import projeto.pucsp.tcc.borda.empresa.recurso.RecursoEmpresa;
import projeto.pucsp.tcc.borda.empresa.servico.Servico;


@Slf4j
@RequestMapping( path = "/empresa")
@RestController
public class Controle implements RecursoEmpresa {

	private final Servico servico;

	public Controle(Servico servico) {
		this.servico = servico;
	}


	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path = "/cadastro")
	@Override
	public IdEmpresa cadastrarEmpresa(@RequestBody EmpresaDto empresaDTO) {

		return servico.cadastrarEmpresa(empresaDTO);
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/{cnpj}")
	@Override
	public EmpresaProxy obterEmpresa(@PathVariable String cnpj) {

		return servico.obterEmpresa(cnpj);
	}

	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping("/{cnpj}")
	@Override
	public IdEmpresa encerrarEmpresa(@PathVariable("cnpj") String cnpj) {

		return servico.encerrarEmpresa(cnpj);
	}


	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/{cnpj}")
	@Override
	public IdEmpresa atualizarEmpresa(@PathVariable("cnpj") String cnpj, @RequestBody EmpresaDto empresaDto) {
		return servico.atualizarEmpresa(cnpj,empresaDto);
	}
}
