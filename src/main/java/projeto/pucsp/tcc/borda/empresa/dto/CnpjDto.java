package projeto.pucsp.tcc.borda.empresa.dto;

import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.validation.constraints.NotBlank;

@Value
@Builder
public class CnpjDto {

	@NotBlank
	@CNPJ
	private String registro;

}
