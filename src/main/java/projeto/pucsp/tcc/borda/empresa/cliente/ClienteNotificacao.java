package projeto.pucsp.tcc.borda.empresa.cliente;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.borda.empresa.entidade.Email;
import projeto.pucsp.tcc.borda.empresa.propriedade.PropriedadesMsgNotificacao;


@Component
@Slf4j
public class ClienteNotificacao {

	private final RabbitTemplate rabbitTemplate;

	private final PropriedadesMsgNotificacao proprieadadesMsgNotificacao;


	public ClienteNotificacao(RabbitTemplate rabbitTemplate, PropriedadesMsgNotificacao proprieadadesMsgNotificacao) {
		this.rabbitTemplate = rabbitTemplate;

		this.proprieadadesMsgNotificacao = proprieadadesMsgNotificacao;
	}


	public void publicar(Email email){


		rabbitTemplate.convertAndSend(proprieadadesMsgNotificacao.getTopicExchange(),email);
		log.info("tentado publicar uma notifcacao");



	}
}
