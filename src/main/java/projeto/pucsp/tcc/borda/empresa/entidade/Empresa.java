package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import projeto.pucsp.tcc.borda.empresa.dto.EmpresaDto;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "empresa")
public class Empresa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "endereco_fk", referencedColumnName = "id")
	private Endereco endereco;

	@NotNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "cnpj_fk", referencedColumnName = "id")
	private Cnpj cnpj;

	@NotBlank
	@Size(max = 15)
	private String telefone;

	@NotBlank
	@Column(name = "nome_empresa")
	private String nomeEmpresa;

	@NotBlank
	@Column(name = "razao_social")
	private String razaoSocial;

	@NotBlank
	@Column(name = "nome_fantasia")
	private String nomeFantasia;


	private Boolean ativo;

	private String token;

	@NotBlank
	private String email;

	@LastModifiedDate
	@Column(name = "data_cadastro")
	private LocalDateTime dataCadastro;

	@Column(name = "data_encerramento")
	private LocalDateTime dataEncerramento;

	@LastModifiedDate
	@Column(name = "data_atualizacao")
	private Timestamp dataAtualizacao;


	public Empresa(EmpresaDto empresaDto) {

		this.cnpj = new Cnpj(empresaDto.getCnpj());
		this.email = empresaDto.getEmail();
		this.endereco = new Endereco(empresaDto.getEndereco());
		this.nomeEmpresa = empresaDto.getNomeEmpresa();
		this.razaoSocial = empresaDto.getRazaoSocial();
		this.nomeFantasia = empresaDto.getNomeFantasia();
		this.dataCadastro = LocalDateTime.now();
		this.telefone = empresaDto.getTelefone();
		this.dataAtualizacao = Timestamp.valueOf(LocalDateTime.now());
		this.ativo = false;


	}

	public void atualizarEmpresa(EmpresaDto empresaDto) {

		this.endereco = new Endereco(empresaDto.getEndereco());
		this.nomeEmpresa = empresaDto.getNomeEmpresa();
		this.razaoSocial = empresaDto.getRazaoSocial();
		this.nomeFantasia = empresaDto.getNomeFantasia();
		this.dataAtualizacao = Timestamp.valueOf(LocalDateTime.now());
		this.telefone = empresaDto.getTelefone();

	}

	public void registarToken(Token token){

		this.token = token.getToken();

	}


	public boolean encerrado() {

		return dataEncerramento != null;

	}

	public void reativar(EmpresaDto empresaDto) {

		dataEncerramento = null;
		ativo = true;
		atualizarEmpresa(empresaDto);

	}

	public void ativarRegistro(){

		ativo= true;

	}

	public void encerrar() {

		ativo = false;
		dataEncerramento = LocalDateTime.now();

	}


}
