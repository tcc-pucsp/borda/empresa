package projeto.pucsp.tcc.borda.empresa.dto;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Value
@Builder
public class EnderecoDto {

	@Size(max = 8)
	@NotBlank
	private String cep;

	@Size(max = 255)
	private String logradouro;

	@Size(max = 30)
	private String complemento;

	@NotNull
	private Integer numero;

	@NotBlank
	private String cidade;
	@NotBlank
	private String estado;





}
