package projeto.pucsp.tcc.borda.empresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import projeto.pucsp.tcc.borda.empresa.propriedade.ProprieadadesMsgGeolocalizacao;
import projeto.pucsp.tcc.borda.empresa.propriedade.PropriedadesMsgNotificacao;

@SpringBootApplication
@EnableConfigurationProperties({ProprieadadesMsgGeolocalizacao.class, PropriedadesMsgNotificacao.class})
public class EmpresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpresaApplication.class, args);
	}

}
