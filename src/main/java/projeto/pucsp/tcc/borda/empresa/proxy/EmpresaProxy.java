package projeto.pucsp.tcc.borda.empresa.proxy;

import org.springframework.beans.factory.annotation.Value;


public interface EmpresaProxy {

	Integer getId();

	String getEmail();

	 @Value("#{target.nomeFantasia}")
	 String getNome();

	@Value("#{target.nomeEmpresa}")
	String getNomeEmpresa();

	@Value("#{target.razaoSocial}")
	String getRazaoSocial();


	EnderecoProxy getEndereco();

	interface EnderecoProxy{

		String getCep();

		String getLogradouro();

		String getComplemento();

		Integer getNumero();

	}

}
