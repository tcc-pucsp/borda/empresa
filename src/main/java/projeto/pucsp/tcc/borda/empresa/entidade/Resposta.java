package projeto.pucsp.tcc.borda.empresa.entidade;

import lombok.Value;
import projeto.pucsp.tcc.borda.empresa.enumeracao.Mensagens;

@Value
public class Resposta {

	Mensagens mensagens;

	String mensagem;

}
