package projeto.pucsp.tcc.borda.empresa.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import projeto.pucsp.tcc.borda.empresa.entidade.Empresa;

import projeto.pucsp.tcc.borda.empresa.proxy.EmpresaProxy;


import java.util.Optional;

public interface Repositorio extends JpaRepository<Empresa, Integer> {
	
	Optional<Empresa> findEmpresaByCnpjRegistro(String cnpj);

	Optional<Empresa> findEmpresaByEnderecoId(Integer id);

	Optional<EmpresaProxy> findEmpresaByCnpjRegistroAndDataEncerramentoIsNull(String cnpj);
}

