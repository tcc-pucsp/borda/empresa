package projeto.pucsp.tcc.borda.empresa;

import org.apache.commons.io.IOUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import projeto.pucsp.tcc.borda.empresa.entidade.Geolocalizacao;
import projeto.pucsp.tcc.borda.empresa.enumeracao.Mensagens;
import projeto.pucsp.tcc.borda.empresa.excecao.EndercoNaoEncontrado;
import projeto.pucsp.tcc.borda.empresa.observador.EmpresaObservador;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmpresaApplicationTests {

	@Autowired
	private MockMvc mockMvc;


	@Autowired
	private EmpresaObservador empresaObservador;

	private static final String URL_EMPRESA = "/empresa/cadastro";
	private static final String URL_GETEMPRESA = "/empresa/";



	private static final String ID_JSON = "$.id";

	private static final String ID_EMPRESA = "1";



	private static final String ID_EMPRESA_DOIS = "2";



	private static final String CODIGO_ERRO = "$.mensagens.codigo";
	private static final String PROTOCOLO = "$.mensagens.contexto";

	private final ClassLoader classLoader = getClass().getClassLoader();






	@Test
	public void cadastrarEmpresa() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresa.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.post(URL_EMPRESA)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isCreated())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(ID_JSON).value(ID_EMPRESA));



		}
	}

	@Test
	public void cadastrarEmpresaValidaComEndereco() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresa2.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.post(URL_EMPRESA)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isCreated())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(ID_JSON).value(ID_EMPRESA_DOIS));

			Geolocalizacao geolocalizacao = new Geolocalizacao();
			geolocalizacao.setEndereco("247, Rua Caio Prado, República, São Paulo, Sao Paulo, São Paulo, Brazil, 01303-001");
			geolocalizacao.setId(2);
			geolocalizacao.setLatitude(-23.549843);
			geolocalizacao.setLatitude(-46.648216);

			empresaObservador.salvarEndereco(geolocalizacao);
		}
	}


	@Test(expected = EndercoNaoEncontrado.class)
	public void processarUmEndereco() {

		Geolocalizacao geolocalizacao = new Geolocalizacao();
		geolocalizacao.setEndereco("247, Rua Caio Prado, República, São Paulo, Sao Paulo, São Paulo, Brazil, 01303-001");
		geolocalizacao.setId(10);
		geolocalizacao.setLatitude(-23.549843);
		geolocalizacao.setLatitude(-46.648216);

		empresaObservador.salvarEndereco(geolocalizacao);


	}



	@Test
	public void cadastrarEmpresaDuplicada() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresa.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.post(URL_EMPRESA)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isConflict())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.EMPRESA_JA_CADASTRADA.getCodigo()))
					.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.EMPRESA_JA_CADASTRADA.getContexto()));

		}

	}

	@Test
	public void cadastrarEmpresaInvalida() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresaInvalida.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.post(URL_EMPRESA)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isBadRequest())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.DADOS_INVALIDOS.getCodigo()))
					.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.DADOS_INVALIDOS.getContexto()));

		}

	}
	@Test
	public void cadastrarEmpresaInvalidaCnpj() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("emprsaInvalidoCpj.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.post(URL_EMPRESA)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isBadRequest())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.DADOS_INVALIDOS.getCodigo()))
					.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.DADOS_INVALIDOS.getContexto()));

		}

	}

	@Test
	public void obterEmpresa() throws Exception {

		mockMvc
				.perform(MockMvcRequestBuilders
						.get(URL_GETEMPRESA + "/01987000000190")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(MockMvcResultMatchers.jsonPath(ID_JSON).value(ID_EMPRESA));

	}

	@Test
	public void obterEmpresaNaoCadastrada() throws Exception {

		mockMvc
				.perform(MockMvcRequestBuilders
						.get(URL_GETEMPRESA + "/514615870007")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isNotFound())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getCodigo()))
				.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getContexto()));

	}



	@Test
	public void destivarEmpresa() throws Exception {

		mockMvc
				.perform(MockMvcRequestBuilders
						.delete(URL_GETEMPRESA + "/86835621000100")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(MockMvcResultMatchers.jsonPath(ID_JSON).value(ID_EMPRESA_DOIS));
	}


	@Test
	public void destivarEmpresaInesistenteOuDestivada() throws Exception {

		mockMvc
				.perform(MockMvcRequestBuilders
						.delete(URL_GETEMPRESA + "/86835621000100")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isNotFound())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getCodigo()))
				.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getContexto()));

	}




	@Test
	public void modificarEmpresaDestivada() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresaUpdate.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.put(URL_GETEMPRESA + "/86835621000100")
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isNotFound())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getCodigo()))
					.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getContexto()));

		}
	}


	@Test
	public void rastrearEmpresaNaoCadastrada() throws Exception {

		mockMvc
				.perform(MockMvcRequestBuilders
						.get(URL_GETEMPRESA + "/86835621000100")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isNotFound())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getCodigo()))
				.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getContexto()));

	}


	@Test
	public void reativarEmpresa() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresa2.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.post(URL_EMPRESA)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isCreated())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(ID_JSON).value(ID_EMPRESA_DOIS));
		}
	}

	@Test
	public void updateEmpresa() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresaUpdate.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.put(URL_GETEMPRESA + "/86835621000100")
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(ID_JSON).value(ID_EMPRESA_DOIS));
		}
	}




	@Test
	public void updateEmpresaNaoCadastrada() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("empresaUpdate.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc
					.perform(MockMvcRequestBuilders
							.put(URL_GETEMPRESA + "/91817948000190")
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(result))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isNotFound())
					.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(MockMvcResultMatchers.jsonPath(CODIGO_ERRO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getCodigo()))
					.andExpect(MockMvcResultMatchers.jsonPath(PROTOCOLO).value(Mensagens.EMPRESA_NAO_ENCONTRADA.getContexto()));

		}
	}





}


