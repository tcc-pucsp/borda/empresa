package projeto.pucsp.tcc.borda.empresa.config;

import com.rabbitmq.client.Channel;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class RabbitMqMockConfig {

	@Bean
	public ConnectionFactory connectionFactory(){

		ConnectionFactory connectionFactory = Mockito.mock(ConnectionFactory.class);

		Connection connection = connection();

		Mockito.when(connectionFactory.createConnection()).thenReturn(connection);

		Mockito.when(connection.isOpen()).thenReturn(true);

		Mockito.when(connection.createChannel(Mockito.anyBoolean())).thenReturn(Mockito.mock(Channel.class));

		return connectionFactory;

	}


	private Connection connection(){


		return Mockito.mock(Connection.class);

	}

}
