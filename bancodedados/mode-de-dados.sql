



create or replace table cnpj
(
    id       int auto_increment primary key,
    registro char(14) not null,
    constraint cnpj_registro_uindex
        unique (registro)
) comment 'Tabela de CNPJ';


create or replace table endereco
(
    id               int auto_increment primary key,
    cep              char(8)       not null,
    logradouro       varchar(255)  not null,
    complemento      varchar(20)   not null,
    numero           int           not null,
    latitude         decimal(9, 6) null,
    longitude        decimal(9, 6) null,
    data_cadastro    datetime      not null default CURRENT_TIMESTAMP,
    data_atualizacao timestamp     not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
)
    comment 'Tabela de Endereços';

create or replace table social
(
    id                int auto_increment primary key,
    endereco_fk       int          not null,
    cpnj_fk           int          not null,
    nome_instituicao  varchar(255) not null,
    email             char(50)     not null,
    data_cadastro     datetime     not null default CURRENT_TIMESTAMP,
    data_encerramento datetime     null,
    data_atualizacao  timestamp    not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    FOREIGN KEY (cpnj_fk) REFERENCES cnpj (id),
    FOREIGN KEY (endereco_fk) REFERENCES endereco (id)
)
    comment 'Tabela de Informações da parte social';


create or replace table lance
(
    id                  int auto_increment
        primary key,
    social_id           int                                 not null,
    alimento_id         int                                 not null,
    situacao_lance      tinyint   default 0                 not null,
    quantidade_alimento int       default 1                 not null,
    data_lance          datetime  default CURRENT_TIMESTAMP not null,
    data_atualizacao    timestamp default CURRENT_TIMESTAMP not null,
    FOREIGN KEY (social_id) REFERENCES social (id)
)
    comment 'Tabela Lance Alimento';


create or replace table empresa
(
    id                int auto_increment primary key,
    endereco_fk       int          not null,
    cnpj_fk           int          not null,
    nome_empresa      varchar(255) not null,
    razao_social      varchar(255) not null,
    nome_fantasia     varchar(255) not null,
    email             char(50)     not null,
    data_cadastro     datetime              default CURRENT_TIMESTAMP not null,
    data_encerramento timestamp    null,
    data_atualizacao  timestamp    not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    FOREIGN KEY (cnpj_fk) REFERENCES cnpj (id),
    FOREIGN KEY (endereco_fk) REFERENCES endereco (id)
)
    comment 'Tabela de Informações da parte empresarial';


create or replace table lote
(
    id              int auto_increment primary key,
    situacao_lote   int          not null default 0,
    alimento_id     int          not null,
    numero_lote     varchar(255) not null,
    data_validade   date     not null,
    data_fabricacao date     not null
) comment 'Tabela de Informações do lote do alimento ofertado';

create or replace table oferta
(
    id                  int auto_increment primary key,
    empresa_fk          int       not null,
    situacao_oferta     int       not null default 0,
    quantidade_alimento int       not null,
    lote_fk             int       not null,
    data_oferta         datetime           default CURRENT_TIMESTAMP not null,
    data_atualizacao    timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    FOREIGN KEY (empresa_fk) REFERENCES empresa (id),
    FOREIGN KEY (lote_fk) REFERENCES lote (id)
) comment 'Tabela de Informações de oferta';


create or replace table transacao
(
    id             int auto_increment primary key,
    lance_fk       int      not null,
    oferta_fk      int      not null,
    data_transacao datetime not null default CURRENT_TIMESTAMP,
    FOREIGN KEY (lance_fk) REFERENCES lance (id),
    FOREIGN KEY (oferta_fk) REFERENCES oferta (id)

) comment 'Tabela de informações da transação estabelecida entre social e empresa';
